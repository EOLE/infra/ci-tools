# Tools for the Gitlab CI

This project provides several YAML files to be
[included](https://docs.gitlab.com/ee/ci/yaml/#include) in your
project [.gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/) file.

[[_TOC_]]

## Features

The CI tools:

- supports up to 3 branches in a release cycles
- provides YAML templates to
  - [validate commit message formatting](docs/GETTING-STARTED.md#validate-commit-messages)
  - [build and tag docker images](docs/GETTING-STARTED.md#build-and-tag-docker-images)
  - [publish releases automatically on push to `$STABLE_BRANCH`](docs/GETTING-STARTED.md#generate-release-with-semantic-version-scheme)

```mermaid
graph LR
    %% User working branch
    Branch([CONTRIB BRANCH])
    BranchCI{{CI}}
    BranchCommitLint(commitlint)
    BranchDockerImage((image))
    BranchDockerTag>docker:git-$CI_COMMIT_SHORT_SHA]

    %% Project `dev` branch
    Dev([DEV])
    DevCI{{CI}}
    DevCommitLint(commitlint)
    DevDockerImage((image))
    DevDockerTag>docker:dev]

    %% Project `testing` branch for prerelease
    Testing([TESTING])
    TestingCI{{CI}}
    TestingCommitLint(commitlint)
    TestingDockerImage((image))
    TestingDockerTag>docker:testing]

    %% Project `stable` branch
    Stable([STABLE])
    StableCI{{CI}}

    %% Project `release` tag
    Release>release/x.y.z]
    ReleaseCI{{CI}}

    ReleaseDockerTag{{docker:x.y.z}}
    MajorDockerTag{{docker:x}}
    MinorDockerTag{{docker:x.y}}
    StableDockerTag{{docker:stable}}
    LatestDockerTag{{docker:latest}}

    Branch          -->|push| BranchCI
    BranchCI        -->|lint| BranchCommitLint
    BranchCI        -->|build| BranchDockerImage
    BranchCI        -->|release| BranchDockerTag
    BranchDockerTag -.- BranchDockerImage
    Branch          -->|merge| Dev

    Dev          -->|push| DevCI
    DevCI        -->|lint| DevCommitLint
    DevCI        -->|build| DevDockerImage
    DevCI        -->|release| DevDockerTag
    DevDockerTag -.- DevDockerImage
    Dev          -->|merge| Testing

    Testing          -->|push| TestingCI
    TestingCI        -->|lint| TestingCommitLint
    TestingCI        -->|build| TestingDockerImage
    TestingCI        -->|release| TestingDockerTag
    TestingDockerTag -.- TestingDockerImage
    Testing          -->|merge| Stable

    Stable      -->|push| StableCI
    StableCI    -->|release| Release

    Release     -->|push| ReleaseCI
    ReleaseCI   -->|release| ReleaseDockerTag
    ReleaseCI   -->|release| MajorDockerTag
    ReleaseCI   -->|release| MinorDockerTag
    ReleaseCI   -->|release| StableDockerTag
    ReleaseCI   -->|release| LatestDockerTag
```

## Use the ci-tools to setup a new CI

There is a dedicated [documentation to start using the `ci-tools`
templates](docs/GETTING-STARTED.md) in your project.

## Contributing to this project

### Commit messages

Commit messages formatting is **significant**!

Please, see [How to contribute](docs/CONTRIBUTING.md) for more details

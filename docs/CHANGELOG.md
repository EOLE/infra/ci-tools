# Changelog

## [1.12.0](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/compare/release/1.11.1...release/1.12.0) (2022-07-05)


### Features

* **maven:** copy package to remote repository with `.maven:deploy` ([b72b0a8](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/b72b0a821fd32c065fc7cf60b3a344003c46b373))
* **maven:** ensure quality criteria are met with `.maven:verify` ([a46a236](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/a46a23683add58623bae5c6ce1b4e8367705923f))
* **maven:** validate the project with `.maven:validate` ([92d822d](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/92d822d48d6bac266f4ffaa891217280d36b68ba))


### Bug Fixes

* **maven:** define `SRC_DIR` variable ([8d8a6af](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/8d8a6af8249923dc027c37cf53c7dec9c6f0dedb))

### [1.11.1](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/compare/release/1.11.0...release/1.11.1) (2022-06-28)


### Bug Fixes

* **.git:commitlint:** workaround loading project configuration ([fde1fc0](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/fde1fc07df5358531df6bfb99ae9aabd279cb490))

## [1.11.0](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/compare/release/1.10.0...release/1.11.0) (2022-06-23)


### Features

* **.git:commitlint:** skip no branch or semantic-release commit ([3a855ab](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/3a855ab00db2a0a8022ad14e4c53e02dc93bd01c))
* **helm:** build the helm package ([115a62a](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/115a62a808a91f9ad2b1aa74775786c3d693b330))
* **helm:** publish the helm package ([98f3a6e](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/98f3a6e473d2fb49d3335deaee99812b2d234103))
* **helm:** verify helm formatting with `.helm:lint` job template ([7e1ccbd](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/7e1ccbd30f8f53fa29bfc574159e52999803f5e7))
* **semantic-release:** don't enforce configuration on job rules ([1a1a9ca](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/1a1a9cac043b2458f9811c76bdc0e15784904eec))
* **semantic-release:** update version of Helm charts ([f776b12](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/f776b127787ba40bbb831a621f3fc76015889aad))


### Bug Fixes

* **helm:** rename jobs to their used helm command ([7d6886c](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/7d6886c5c9edd79f310cb7ff6900c0837079c411))


### Code Refactoring

* **semantic-release:** move rules in main configuration file ([2ba784d](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/2ba784d805aefc92d60559330b78f5edf9c986e2))


### Documentation

* **getting-started:** update `semantic-release` configuration steps ([b6d5bc5](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/b6d5bc56c08e2526186f32bf5daf7a8b42bb087b))


### Continuous Integration

* **build:** new docker image with required helm tools ([e620568](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/e62056859609fb3f18c718cc06f9349299b97ca2))

## [1.10.0](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/compare/release/1.9.1...release/1.10.0) (2022-03-22)


### Features

* **semantic-release:** comment merge requests and issues in release ([bc387d4](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/bc387d41916a0f2a299022a40570ed8967d3aa3a))

### [1.9.1](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/compare/release/1.9.0...release/1.9.1) (2022-03-22)


### Bug Fixes

* **python:** backquotes in shell strings try to execute commands ([e9cde63](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/e9cde63792ee1e0081e7e6b744a0ac64c8313154))

## [1.9.0](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/compare/release/1.8.0...release/1.9.0) (2022-03-21)


### Features

* **python:** use `build` module to support PEP517 package building ([a45db53](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/a45db53855b9827309e864df891acbc8c7a7dedb))

## [1.8.0](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/compare/release/1.7.0...release/1.8.0) (2022-03-18)


### Features

* **python:** build source distribution package ([1ee7498](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/1ee7498b836bc68166d46f92b1624958a61382b9))
* **python:** build wheel package ([6b6a59f](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/6b6a59f217dce7471dfa77611a13495b78c0cbc0))
* **python:** check code formatting using `black` ([4c9f045](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/4c9f0456851edf1d4303f833c61848ac99010ab5))
* **python:** upload python package files with `twine` ([8a1d9d4](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/8a1d9d4cfb21b4fbe3fee2a26fc8f786f933e2af))

## [1.7.0](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/compare/release/1.6.0...release/1.7.0) (2022-03-15)


### Features

* **semantic-release:** switch to `conventionalcommits` changelog ([07950c4](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/07950c4c09e7f919d8597e95c51b91315fdcb2e1))

# [1.6.0](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/compare/release/1.5.0...release/1.6.0) (2022-03-15)


### Features

* **git:** new `.git:merge-to` job template to merge a reference ([f8501fc](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/f8501fc7e3301e9f30718faef2cdf9021cf80c62))

# [1.5.0](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/compare/release/1.4.0...release/1.5.0) (2022-02-16)


### Documentation

* **getting-started:** generate prerelease version for `testing` ([733fb66](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/733fb661a574ab4de834c062796bc8376f8fe35f))


### Features

* **commitlint:** update `commitlint` to version 16 ([d003342](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/d003342a376965f7dbfa7c6baf1edc124912ffc2))
* **docker:** tag docker image of contributor branches ([1ddb17e](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/1ddb17e4589e44055dbf7213189813d68078cc15))
* **semantic-release:** new job templates for prerelease ([879854f](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/879854fe5ac7e6d6af508a42a32f79fa9173044d))
* **semantic-release:** update to version `19` ([92aceec](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/92aceec996568a67eef5b19221ec0be4ea1277dd))

# [1.4.0](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/compare/release/1.3.0...release/1.4.0) (2022-01-24)


### Features

* **rules:** support prerelease tags for `semantic-release` ([0ec996a](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/0ec996ac0072235cdca3fc51c2a1eb611d3b1665))
* **semantic-release/config:** easier declaration of branches ([c7acd28](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/c7acd28195c7effa730ff20e2bc6de1d2f6991fc))
* **semantic-release:** new rules for prerelease ([b2752bd](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/b2752bd900d23c554adf07b1feb54597f0dbd968))

# [1.3.0](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/compare/release/1.2.1...release/1.3.0) (2022-01-11)


### Bug Fixes

* **semantic-release:** the branch must be `stable` ([4b9152d](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/4b9152dcae29a8cd792de452041d47da6bf6807c))


### Code Refactoring

* **commitlint:** the upstream extra repository is useless ([cdff4ab](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/cdff4abd8af608b8ab31398c0a6703a50eae0e08))


### Documentation

* **commitlint:** describe usage of the commitlint job ([8b3749f](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/8b3749f044227c390e15480c63f125c24e99767a))
* **getting-started:** quick setup and detailed explanations ([007aa21](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/007aa2195252d8140d8ecd4438f231a973aa30e9))
* **readme:** demonstrate a possible release cycle ([8d02a0c](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/8d02a0cf712cb590cfc3a65d7231a9157bcbbbb6))


### Features

* **commitlint:** users can configure image with `$COMMITLINT_IMAGE` ([bc7f19c](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/bc7f19c3be02a00e03a39ffe31b4fecbd5454964))
* **commitlint:** users can configure the base branch ([612dc3e](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/612dc3ea47080f3213cc6610ec321b4c57fa8e72))
* **docker:** .build-docker-image must accept kaniko arguments ([bd5c77e](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/bd5c77eabbc03fec73b9405ef392197a5029ae29))
* **semantic-release:** don't build changelog on prerelease branches ([5a02f5d](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/5a02f5d23aca195cb18f2e0d865eaf4c13b7351f))
* **semantic-release:** manage python projects releases ([9794bcb](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/9794bcb0b7d427af10e99a65723456953f575182))


### Styles

* **js:** js configuration files do not pass eslint ([5874dca](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/5874dca35effe04f82ff848918706a15012dcf73))

## [1.2.1](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/compare/release/1.2.0...release/1.2.1) (2021-12-09)


### Code Refactoring

* **rules:** permit to combine individual rules ([075712a](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/075712a9d7e7d9cac8731debb0312a092bcefbb2)), closes [#3](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/issues/3)

# [1.2.0](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/compare/release/1.1.0...release/1.2.0) (2021-12-06)


### Features

* **rules:** we want to skip pipelines on more keywords ([0a24edb](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/0a24edbeb188efe3163e28b75cc42e9d1ed21c2e)), closes [#2](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/issues/2)
* **runners:** apps.education projects will use dedicated runners ([3f447c4](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/3f447c40f4d60d84a93e59d14d70c274edc6465b)), closes [#1](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/issues/1)

# [1.1.0](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/compare/release/1.0.0...release/1.1.0) (2021-11-30)


### Features

* **semantic-release:** use the image build by CI itself ([ba0d024](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/ba0d02416182df2e8ef66d1f4613c95d7ff841ec))

# 1.0.0 (2021-11-30)


### Continuous Integration

* **codeowners:** define @EOLE group members as owners ([5f57f4a](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/5f57f4a0033b7a322077696ee2541b10c211892a))
* **commitlint:** master is not always the repository default branch ([48b9f29](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/48b9f29c2d66551e19da00aaf96e16ee1e7d04cd))
* **gitlab-ci:** split in reusable templates ([b39e36b](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/b39e36bd760646385dc8021b5699e61171bbcba1))
* **gitlab:** add commit lint and its documentation ([dfea847](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/dfea8470837e6d77a3c1098e9d00fd25a44b5bf4))
* **rules:** define git-flow style rules ([e8e12e8](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/e8e12e8f2e5aca1eda5798726888b1361ebd1821))


### Features

* **docker:** build images and push them to `${CI_REGISTRY}` ([a18d1f9](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/a18d1f90391c91f517adb102e285d3e0460d1f34))
* **docker:** tag images based on release cycle ([b82a232](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/b82a232ee86a8e9627c116e3a5357fd451170407))
* **gitlab-ci:** automatic release creation with semantic-release ([df92290](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/df9229099f9f7545796bde74ec2c67e2ed10d969))
* **rules:** we prefer release naming over production status ([b7c0cd0](https://gitlab.mim-libre.fr/EOLE/infra/ci-tools/-/commit/b7c0cd0ef2f9cff3c0748f1d57927395013c153f))
